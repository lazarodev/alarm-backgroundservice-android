package com.gt.dev.lazaro.alarmservicebackgroundexample;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by lazarodev on 10/9/2017.
 */

public class SensorRestarterBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        Log.i(SensorRestarterBroadcastReceiver.class.getSimpleName(), "Service Stops! Ooooooooooooopppppssss!!!");

        context.startService(new Intent(context, SensorService.class));

    }
}
