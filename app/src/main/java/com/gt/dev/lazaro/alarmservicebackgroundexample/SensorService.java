package com.gt.dev.lazaro.alarmservicebackgroundexample;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by lazarodev on 10/9/2017.
 */

public class SensorService extends Service {

    public int counter = 0;

    public SensorService(Context applicationContext) {
        super();
        Log.i("HERE", "Here i am!");
    }

    public SensorService() {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        super.onStartCommand(intent, flags, startId);
        startTimer();

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("EXIT", "onDestroy!");
        Intent broadCastIntent = new Intent("lazaro.dev.gt.com.ActivityRecognition.RestartSensor");
        sendBroadcast(broadCastIntent);
        stopTimerTask();
    }

    private Timer timer;
    private TimerTask timerTask;
    long oldTime = 0;

    public void startTimer() {
        // set a new timer
        timer = new Timer();

        // initilalize the timerTask job
        initilizeTimerTask();

        // schedule the timer, to wake u p every 1 second
        timer.schedule(timerTask, 1000, 1000);
    }

    public void initilizeTimerTask() {
        timerTask = new TimerTask() {
            @Override
            public void run() {
                Log.i("in timer", "in timer ++++ " + (counter++));
            }
        };
    }

    public void stopTimerTask() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
